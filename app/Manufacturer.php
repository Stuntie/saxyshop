<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    
    /**
     * Relationships.
     *
     * @return void
     */
    public function products(){
        return $this->hasMany('App\Product');
    }
}
