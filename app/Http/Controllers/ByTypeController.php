<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Manufacturer;
use App\Type;

class ByTypeController extends Controller
{    
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Products = Product::all()->sortBy('name');
        return view('type')->with('Products', $Products);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Products = Product::where('type', $id)->paginate(5);
        $Manufacturers = Manufacturer::all();
        return view('byType')->with('selectedType', $id)->with('Products', $Products)->with('Manufacturers', $Manufacturers);
    }
}
