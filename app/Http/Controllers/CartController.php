<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CartController extends Controller
{
      
    /**
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth' );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$Orders = Order::orderBy('name', 'asc')->paginate(10);
        //return view('orders.index')->with('Orders', $Orders);
        return view('cart.cart');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function submit()
    {
        //$Orders = Order::orderBy('name', 'asc')->paginate(10);
        //return view('orders.index')->with('Orders', $Orders);
        return view('cart.submit');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        //$Orders = Order::orderBy('name', 'asc')->paginate(10);
        //return view('orders.index')->with('Orders', $Orders);
        return view('cart.cart');
    }
}
