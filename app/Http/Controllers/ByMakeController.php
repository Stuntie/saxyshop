<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Manufacturer;
use App\Type;

class ByMakeController extends Controller
{    
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Manufacturers = Manufacturer::all()->sortBy('name');
        return view('make')->with('Manufacturers', $Manufacturers);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Manufacturer = Manufacturer::where('slug','=', $id)->first();
        $Products = Product::where('manufacturer_id', $Manufacturer->id)->paginate(5);
        return view('byMake')->with('selectedMaker', $id)->with('Products', $Products)->with('Manufacturer', $Manufacturer);
    }
}
