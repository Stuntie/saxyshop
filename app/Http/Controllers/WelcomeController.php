<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Manufacturer;
use App\Type;

class WelcomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $Manufacturers = Manufacturer::all();
        $Types = Type::all();
        return view('welcome')->with('Manufacturers', $Manufacturers)->with('Types', $Types);
    }
}
