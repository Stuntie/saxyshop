<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('slug'); // alternate ID value
            $table->string('name');
            $table->string('type');
            $table->text('description')->nullable();
            $table->decimal('price', 10,2)->default(0); // Stored in pounds and pence. 0 or Null = TBC 'please ring'.
            $table->text('options');
            $table->string('image')->default('default.jpg');
            $table->integer('manufacturer_id'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
