@extends('layouts.app')

@section('content')

<h1>Saxophones by Type: {{ $selectedType }} Saxes.</h1>

<p>
    @if(count($Products) > 0)
        @foreach($Products as $Product)

        @include('inc.sax_card')
 
        @endforeach

        {{$Products->links()}}

    @else
        <p>no Products were found</p>
    @endif                       
    
</p>

@endsection