@extends('layouts.app')

@section('content')

<h1>Saxophones by Brand: {{ $selectedMaker }} Saxes.</h1>

@include('inc.brand_card')

<p>
    @if(count($Products) > 0)
        @foreach($Products as $Product)
        
            @include('inc.brand_sax_card')
 
        @endforeach
        {{$Products->links()}}
    @else
        <p>no Products were found</p>
    @endif                       
    
</p>

@endsection