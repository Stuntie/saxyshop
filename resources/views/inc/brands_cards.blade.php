@foreach($Manufacturers->Chunk(3) as $ManufacturersChunk)
    <div class="card-deck">  
        @foreach ($ManufacturersChunk as $Manufacturer)
            <div class="card">
                @include('inc.brand_card')
            </div>
        @endforeach
    </div>
@endforeach
