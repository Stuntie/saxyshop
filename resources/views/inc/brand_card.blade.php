<div class="card">      
    <div class="card-body">
    <h5 class="card-title"><a href="##" class="">{{ $Manufacturer->name}} ({{ $Manufacturer->slug}})</a></h5>
    <p class="card-text"><img src="/img/logos/{{$Manufacturer->logo}}" style="max-width: 200px;"/></p>
    <p class="card-text">{{ $Manufacturer->description}}</p>
    </div>
</div>