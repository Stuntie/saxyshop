<div class="card">      
    <div class="card-body">
    <h5 class="card-title"><a href="##" class="">{{ $Product->name}} ({{ $Product->slug}})</a></h5>
    <p class="card-text"><img src="/img/products/{{$Product->image}}" style="max-width: 200px;"/></p>
    <p class="card-text">
        @if($Manufacturers)
            @if(count($Manufacturers) > 0)
                @foreach($Manufacturers as $Manufacturer) 
                    @if($Product->manufacturer_id == $Manufacturer->id) 
                    {{ $Manufacturer->name}}
                    @endif
                @endforeach 
            @endif   
        @endif
    </p>
    <p class="card-text">{{ $Product->type }} &pound;{{ $Product->price }}</p>
    <p class="card-text">{{ $Product->description}}</p>
    </div>
    <div class="card-footer">
        <p>Quantity</p>
    <a href="#" class="btn btn-primary w-100">Add to Cart</a>
    </div>
</div>