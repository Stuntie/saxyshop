<div class="card-deck">        
    <div class="card">
      
    <svg class="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image cap">
        <title>Placeholder</title><rect width="100%" height="100%" fill="#868e96"></rect><text x="50%" y="50%" fill="#dee2e6" dy=".3em">Image cap</text>
    </svg>
      <div class="card-body">
        <h5 class="card-title"><a href="/byType/Baritone" class="">Alto Saxophones</a></h5>
        <p class="card-text">The classic saxophone with it's Alto sweet sound. One of the most versitile and widely used in both Jazz, Rock and Pop as well as Classical.</p>
      </div>
      <div class="card-footer">
        <a href="/byType/Alto" class="btn btn-primary w-100">View our Alto range</a>
      </div>
    </div>

    <div class="card">
      
    <svg class="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image cap">
        <title>Placeholder</title><rect width="100%" height="100%" fill="#868e96"></rect><text x="50%" y="50%" fill="#dee2e6" dy=".3em">Image cap</text>
    </svg>

      <div class="card-body">
        <h5 class="card-title"><a href="/byType/Baritone" class="">Tenor Saxophones</a></h5>
        <p class="card-text">Coolness and sensuality personified. There is a reason they call the Sax 'the Devil's horn' - and this is why!</p>
      </div>
      <div class="card-footer">
        <a href="/byType/Tenor" class="btn btn-primary w-100">View our Tenor range</a>
      </div>
    </div>

    <div class="card">
      
    <svg class="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image cap">
        <title>Placeholder</title><rect width="100%" height="100%" fill="#868e96"></rect><text x="50%" y="50%" fill="#dee2e6" dy=".3em">Image cap</text>
    </svg>
      <div class="card-body">
        <h5 class="card-title"><a href="/byType/Baritone" class="">Soprano</a></h5>
        <p class="card-text">Do high and indulge your inner Kenny G. Or Sidney Bechet, if Jazz is your thing.</p>
      </div>
      <div class="card-footer">
        <a href="/byType/Soprano" class="btn btn-primary w-100">View our Soprano range</a>
      </div>
    </div>

    <div class="card">
      
    <svg class="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image cap">
        <title>Placeholder</title><rect width="100%" height="100%" fill="#868e96"></rect><text x="50%" y="50%" fill="#dee2e6" dy=".3em">Image cap</text>
    </svg>
      <div class="card-body">
        <h5 class="card-title"><a href="/byType/Baritone" class="">Baritone</a></h5>
        <p class="card-text">The bigger and bassier brothr to the Alto. For when you want to go low and powerful.</p>
      </div>
      <div class="card-footer">
        <a href="/byType/Baritone" class="btn btn-primary w-100">View our Baritone range</a>
      </div>
    </div>
  </div>