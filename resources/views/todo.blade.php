@extends('layouts.app')

@section('content')
    <h1>Saxy Shop ToDo list</h1>

    <p>NOTE: This is for Demo purposes - it would not appear in a Live Product.</p>
    
    <p><a href="/spec" class="btn btn-secondary">Tech Spec</a></p>

    <p>This is a Demo app with the build still in progress. 
        So this page aims to gather the To Do and optional elements together. 
        So you can better understand what is missing, or not implemented, and why
    </p>

    <h3>To Do - Spec Task List</h3>
    <p>These are items needed to fullfil the <a href="/spec">Tech Spec</a></p>
    <ul>
        <li>... To Do: coming soon </li>
        
    </ul>

    <h3>To Do - Extra Task List</h3>
    <p>These are items not needed to fullfil the <a href="/spec">Tech Spec</a>,
    but which I plan to do.</p>
    <ul>
        <li>... To Do:coming soon....</li>
    </ul>

    <h3>Not Implemented Task List</h3>
    <p>These are items that are not in the <a href="/spec">Tech Spec</a> (as it's just a Demo). 
        So are not currently implemented. But which would normally be added as part a full project</p>
    <ul>
        <li>No eCommerce system<br>
        This is a Demo app, so no actual eCommerce system is used. We are just looking at the PHP site parts here.</li>
        <li> To Do: coming soon...</li>
    </ul>
@endsection
