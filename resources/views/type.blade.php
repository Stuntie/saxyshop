@extends('layouts.app')

@section('content')
    <h1>Saxy Shop  - Types</h1>
    <p>View by Saxophone Types. </p>
    
    <p><strong>Saxes by type:</strong><br>
        <a href="/byType/Soprano">Soprano</a><br>
        <a href="/byType/Alto">Alto</a><br>
        <a href="/byType/Tenor">Tenor</a><br>
        <a href="/byType/Baritone">Baritone</a><br>
    </p>
   
@endsection