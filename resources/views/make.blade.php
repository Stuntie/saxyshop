@extends('layouts.app')

@section('content')
    <h1>Saxy Shop - Brand</h1>
    
    <p><strong>Saxes by Brand:</strong><br>
        @if(count($Manufacturers) > 0)
            @foreach($Manufacturers as $Manufacturer) 
                <a href="/byMake/{{ $Manufacturer->slug}}">{{ $Manufacturer->name}}</a><br>
            @endforeach 
        @else
            <p>no Manufacturer details were found</p>
        @endif 
    </p>
@endsection
