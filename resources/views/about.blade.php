@extends('layouts.app')

@section('content')
    <h1>About Saxy Shop...</h1>

    <p>
    This is a Demo app for a basic Laravel shop type app.</p>
    <p>Uses Saxophones as...<br>
    A) I like Saxes. 
    <br> B) Allows me to play with the permutations e.g. Brand/(id)/Type(id) and Type/(id)/Brand(id) etc.</p>
    <p>Allows Customers to view and 'buy' Saxophones of various types from Ficticious companies.</p>
    <p>(Note: no actual eCommerce system is used in the Demo. Any Card 'transactions are faked with dummy data etc. And are not real ').</p>
    <ul>
        <li>View by Type - Alto Tenor etc.</li>
        <li>View by Saxophone Company.</li>
        <li>Register a Customer Profile.</li>
        <li>View Customer Profile details.</li>
        <li>View Customer orders.</li>
        <li>Use a cart to add and buy orders.</li>

    </ul>
@endsection
