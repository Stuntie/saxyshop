@extends('layouts.app')

@section('content')
<div>
    <h1>Saxy Shop Website</h1>
    <p>View and buy our lovely Saxophones</p>

    @include('inc.carousel')

    <hr>
    <h2>By Saxophone type</h2>
    @include('inc.types_cards')

    <hr>
    <h2>By Brand</h2>
    @include('inc.brands_cards')

</div>
@endsection
