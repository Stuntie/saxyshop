<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index')->name('welcome');


Route::get('/about', function () {
    return view('about');
});
Route::get('/todo', function () {
    return view('todo');
});
Route::get('/spec', function () {
    return view('spec');
});


Auth::routes();

Route::get('/dashboard', 'dashboardController@index')->name('dashboard');

/*
*
* Main display pages
*
*/


/*
*
* Products by Type pages
*
*/

Route::get('/type', function () {
    return view('type');
});

Route::get('/byType', function () {
    return view('type');
});

Route::get('/byType/{id}', 'ByTypeController@show');

/*
*
* With by Make pages
*
*/

Route::get('/make', 'ByMakeController@index');

Route::get('/make2', function () {
    return view('make');
});

Route::get('/byMake', function () {
    return view('make');
});

Route::get('/byMake/{id}', 'ByMakeController@show');

/*
*
* With Auth protection - Customer Profile and Orders pages
*
*/

/*
*
* Carts pages
*
*/
Route::get('/cart', function () {
    return view('cart.cart');
});
Route::get('/cart/cart', function () {
    return view('cart.cart');
});
Route::get('/cart/submit', function () {
    return view('cart.submit');
});